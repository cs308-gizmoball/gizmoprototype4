package View;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import Model.Absorber;
import Model.Ball;
import Model.Circle;
import Model.Gizmo;
import Model.GridLocation;
import Model.LeftFlipper;
import Model.Model;
import Model.RightFlipper;
import Model.Square;
import Model.Triangle;

public class DisplayWindow extends JFrame implements KeyListener{
	
	private int width = 20, height = 20;
	private int l=25;
	private Model model;
	public DisplayWindow(Model m){
		this.setTitle("Load Prototype");
		this.model=m;
		this.setSize(height*l,width*l+22);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setResizable(false);
		this.add(new PaintPanel());
		addKeyListener(this);
		

		
		
		
	}
	public class PaintPanel extends JPanel{
		public void paint(Graphics draw){
			super.paint(draw);
			
			Color blue = new Color(0, 0, 255);
			Color black = new Color(0, 0, 0);
			Color red = new Color(255, 0, 0);
			
			draw.setColor(black);
			draw.fillRect(0, 0, this.getWidth(), this.getHeight());
			
			int gizX, gizY, gizH, gizW;
			
			for(Gizmo G : model.getGizmos()){
				
				draw.setColor(blue);
				if (G instanceof Circle){
					gizH = G.getWidth();
					gizW = G.getHeight();
					gizX=G.getX();
					gizY=G.getY();
					draw.fillOval(gizX*l, gizY*l, gizW, gizH);
				}
				draw.setColor(red);
				if (G instanceof Square){
					gizH = G.getWidth();
					gizW = G.getHeight();
					gizX=G.getX();
					gizY=G.getY();
					draw.fillRect(gizX*l, gizY*l, gizW, gizH);
				}
				draw.setColor(Color.CYAN);
				if (G instanceof Triangle){
					
					int[]xPoints=((Triangle) G).getXCos();
					xPoints[0]= xPoints[0]*l;
					xPoints[1]= xPoints[1]*l;
					xPoints[2]= xPoints[2]*l;
					int[]yPoints=((Triangle) G).getYCos();
					yPoints[0]= yPoints[0]*l;
					yPoints[1]= yPoints[1]*l;
					yPoints[2]= yPoints[2]*l;
					
					draw.fillPolygon(xPoints, yPoints, 3);
					//draw.fillPolygon(((Triangle) G).getXCos(), ((Triangle) G).getYCos(), 3);
					
				}
				draw.setColor(Color.YELLOW);
				if (G instanceof RightFlipper){
					gizH = G.getWidth();
					gizW = G.getHeight();
					gizX= G.getX();
					gizY= G.getY();
					
					draw.fillRoundRect(gizX*l+40, gizY*l, gizH, gizW, 10, 5);
					
					
				}
				if (G instanceof LeftFlipper){
					gizH = G.getWidth();
					gizW = G.getHeight();
					gizX= G.getX();
					gizY= G.getY();
					draw.fillRoundRect(gizX*l, gizY*l, gizH, gizW, 10, 5);
				}
				draw.setColor(Color.PINK);
				if (G instanceof Absorber){
					gizH = G.getWidth();
					gizW = G.getHeight();
					
					gizX=G.getX();
					gizY=G.getY();
					draw.fillRect(gizX*l, gizY*l, gizH, gizW);
				}
				
			}
			draw.setColor(Color.GREEN);
			for (Ball b : model.getBalls()){
				double xPosition = b.getX()*l;
				double yPosition = b.getY()*l;
				draw.fillOval((int)xPosition, (int)yPosition, l/2, l/2);
			}
	}
	
	}
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}}
