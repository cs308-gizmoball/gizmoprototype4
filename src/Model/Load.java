package Model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Load {
	Model loadModel;
	private BufferedReader fileInput;
	private String gameFileName;
	public Load(Model mod, String file){
		this.loadModel=mod;
		this.gameFileName = file;
		try {
			loadGame();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void loadGame() throws IOException{
		try {
			fileInput = new BufferedReader(new FileReader("testLoad.txt"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String line = fileInput.readLine();
		
		Scanner input = new Scanner(fileInput);
		while (line != null){ 
			
			StringTokenizer st = new StringTokenizer(line);

			
			if (!st.hasMoreTokens()) {
				line = fileInput.readLine();
				continue;
			}

			
			
			String tok1 = st.nextToken();
			
			if(tok1.equals("Square")||tok1.equals("Triangle")||tok1.equals("Circle")||tok1.equals("LeftFlipper")||tok1.equals("RightFlipper")){
				String id = st.nextToken();
				int xCo= Integer.parseInt(st.nextToken());
				int yCo=Integer.parseInt(st.nextToken());
				
				loadModel.addShape(tok1, id, xCo, yCo);
			}
			if(tok1.equals("Rotate")){
				Gizmo GizmoChange = loadModel.getGizmo(st.nextToken());
				
				((Triangle) GizmoChange).rotate();
			}
			if(tok1.equals("Delete")){
				loadModel.deleteGizmo(st.nextToken());
				
			}
			if(tok1.equals("Connect")){
//				Gizmo g1 =loadModel.getGizmo(st.nextToken());
//				Gizmo g2 =loadModel.getGizmo(st.nextToken());
//				g1.addConnection(g2);
//				g2.addConnection(g1);
			}
			if (tok1.equals("KeyConnect")){
				
			}
			if(tok1.equals("Move")){
				
			}
			if (tok1.equals("Ball")){
				String id = st.nextToken();
				double x,y,horVelo, verVelo;
				x = Double.parseDouble(st.nextToken());
				y = Double.parseDouble(st.nextToken());
				horVelo= Double.parseDouble(st.nextToken());
				verVelo = Double.parseDouble(st.nextToken());
				loadModel.addBall(id,x,y,horVelo,verVelo);
				
			}
			if(tok1.equals("Absorber")){
				String id = st.nextToken();
				int x1= Integer.parseInt(st.nextToken());
				int y1=Integer.parseInt(st.nextToken());
				int x2= Integer.parseInt(st.nextToken());
				int y2=Integer.parseInt(st.nextToken());
				loadModel.addAbsorber(id, x1, y1, x2, y2);
				
			}
			line = fileInput.readLine();}
	}
}
