package Model;

public class Absorber extends Gizmo {
	int x1,y1,x2,y2;
	int[] xCos,yCos;
	String name;
	Absorber(String n, int x1, int y1, int x2, int y2){
		xCos = new int[3];
		yCos = new int[3];
		this.setWidth((x2-x1)*25);
		this.setHeight(25);
		this.x1=x1;
		this.y1=y1;
		this.x2=x2;
		this.y2=y2;
		this.name=n;
	}
	@Override
	public int getX(){
		return x1;
	}
	@Override
	public int getY(){
		return y1;
	}
}
