package Model;

public interface GizmoI {
	
	int getX();
	
	int getY();
	
	int getWidth();
	
	int getHeight();
}
