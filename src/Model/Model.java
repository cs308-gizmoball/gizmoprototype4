package Model;

import java.util.ArrayList;
import java.util.Observable;

public class Model extends Observable{
	private ArrayList<Line> lines;
	protected ArrayList<Gizmo> Gizmos;
	protected ArrayList<Ball> Balls;

	public Model(){
		lines = new ArrayList<Line>();
		Gizmos = new ArrayList<Gizmo>();
		Balls = new ArrayList<Ball>();

	}
	
	public ArrayList<Line> getLines() {
		return lines;
	}
	
	public void addLine(Line l) {
		lines.add(l);
	}
	
	public Gizmo getGizmo(String id){
		for (Gizmo g: Gizmos){
			if (g.getID().equals(id)){
				return g;
			}
					
		}
		return null;
	}
	
	public void deleteGizmo(String id){
		Gizmos.remove(getGizmo(id));	
	}
	
	public void addShape(String type, String id, int xCo, int yCo){
		
		if(type.equals("Square")){
			addSquare(id, xCo, yCo);
			
		}
		if (type.equals("Triangle")){
			addTriangle(id,xCo, yCo);
		}
		if (type.equals("Circle")){
			addCircle(id,xCo,yCo);
		}
		if (type.equals("RightFlipper")){
			addRF(id,xCo,yCo);
		}
		if (type.equals("LeftFlipper")){
			addLF(id,xCo,yCo);
		}
	}
	public void addSquare(String id, int xCo, int yCo){
		Square newSquare = new Square(id, xCo, yCo);
		Gizmos.add(newSquare);
	}
	
	public void addTriangle(String id, int xCo, int yCo){
		Triangle newTriangle = new Triangle(id, xCo, yCo);
		Gizmos.add(newTriangle);
		
	}
	public void addCircle(String id, int xCo, int yCo){
		Circle newCircle = new Circle(id, xCo, yCo);
		Gizmos.add(newCircle);
	}
	public void addRF(String id, int xCo, int yCo){
		RightFlipper newRF = new RightFlipper(id, xCo, yCo);
		Gizmos.add(newRF);
	}
	public void addLF(String id, int xCo, int yCo){
		LeftFlipper newLF = new LeftFlipper(id, xCo, yCo);
		Gizmos.add(newLF);
	}
	public void addAbsorber(String id, int x1,int y1, int x2,int y2){
		Absorber newAb= new Absorber(id,x1,y1,x2,y2);
		Gizmos.add(newAb);
	}
	public ArrayList<Gizmo> getGizmos(){
		return Gizmos;
	}

	public ArrayList<Ball> getBalls(){
		return Balls;
	}
	public void addBall(String id, double x, double y, double horVelo, double verVelo) {
		Ball newBall = new Ball(id, x, y, horVelo, verVelo);
		Balls.add(newBall);
		
	}


}
