package Model;

public class Triangle extends Gizmo {
	int[] xCos,yCos;
	int rotateCount;
	
	public Triangle(String n, int x, int y) {
		
		
		xCos = new int[3];
		yCos = new int[3];
		rotateCount=0;
		this.xCo=x;
		this.yCo=y;
		this.name=n;
		setPoints(xCo,yCo);
	}
	private void setPoints(int x, int y){
		
		this.xCos[0]=x;
		this.xCos[1]=x;
		this.xCos[2]=x+1;
		
		this.yCos[0]=y;
		this.yCos[1]=y+1;
		this.yCos[2]=y;
	}
	public int[] getXCos(){
		return xCos;
	}
	public int[] getYCos(){
		return yCos;
	}
	
	public void rotate(){
		System.out.println();
		if(rotateCount==0){
			xCos[1]+=1;
			yCos[1]-=1;
			yCos[2]+=1;	
		}
		else
		if(rotateCount==1){
			yCos[0]+=1;
		}
		else if(rotateCount==2){
			xCos[1]-=1;
			yCos[0]-=1;
			yCos[1]+=1;
		}else if(rotateCount==3){
			yCos[2]-=1;
			rotateCount=-1;
		}
		rotateCount++;
		
	}

}
