package Model;

import java.util.ArrayList;

public class Gizmo implements GizmoI {
		protected int xCo,yCo;
		
		private int width;
		private int height;
		protected String name;
		protected int rotation=0;
		private ArrayList<Gizmo> connections = new ArrayList<Gizmo>();
		private ArrayList<Integer> keypress = new ArrayList<Integer>();
		public String getID(){
			return name;
		}
		public void addKey(int keyCode){
			keypress.add(keyCode);
		}
		public void addConnection(Gizmo connect){
			connections.add(connect);
		}
		public void move(int newX, int newY){
			this.xCo = newX;
			this.yCo = newY;
		}
		@Override
		public String toString(){
			return name;
		}
		@Override
		public boolean equals(Object obj){
			if(((Gizmo) obj).getID().equals(this.name)){
				return true;
			}else{
				return false;
			}
		}
		
		@Override
		public int getX() {
			return xCo;
		}
		@Override
		public int getY() {
			return yCo;
		}
		@Override
		public int getWidth() {
			return width;
		}
		@Override
		public int getHeight() {
			return height;
		}
		public void setWidth(int width) {
			this.width = width;
		}
		public void setHeight(int height) {
			this.height = height;
		}
}
