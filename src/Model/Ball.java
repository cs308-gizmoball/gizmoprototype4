package Model;

import java.util.Observable;

public class Ball extends Observable{
	String name;
	double x, y, xv, yv;

	public Ball(String id, double x, double y, double xv, double yv){
		this.name=id;
		this.x = x;
		this.y = y;
		this.xv = xv;
		this.yv = yv;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}
	
	public void setY(double y) {
		this.y = y;
	}
	
	
	
	
	public double getXv(){
		return xv;
	}
	
	public void setXv(double xv){
		this.xv = xv;
	}
	
	public double getYv(){
		return yv;
	}
	
	public void setYv(double yv){
		this.yv = yv;
	}
	
	
}
