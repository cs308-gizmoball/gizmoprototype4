package Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class GridLocation extends Observable {
	
	List<Ball> balls;
	List<Absorber> absorbers;
	
	public GridLocation(){
		balls = new ArrayList<Ball>();
		absorbers = new ArrayList<Absorber>();
		
	}
	
	public List<Absorber> getAbsorbers(){
		return absorbers;
	}
	
	public void addAbsorber(Absorber absorber){
		absorbers.add(absorber);
		}

	public List<Ball> getBalls() {
		return balls;
	}
	
	public void addBall(Ball ball){
		balls.add(ball);
		}

}
