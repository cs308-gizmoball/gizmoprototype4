package Model;

import physics.LineSegment;

public class Line {
	
	private int xpos;
	private int ypos;
	private int width;
	private LineSegment lineSeg;
	
	
	public Line(int x, int y, int w){
		x = xpos;
		y = ypos;
		w = width;
		lineSeg = new LineSegment(x, y, x + w, y);

		
	}


	public int getXpos() {
		return xpos;
	}



	public int getYpos() {
		return ypos;
	}


	public int getWidth() {
		return width;
	}

	public LineSegment getLineSeg() {
		return lineSeg;
	}


}
